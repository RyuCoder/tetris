"""
Author : RyuCoder
Website: http://ryucoder.in/
Purpose : Program for a text based Tetris game 
Problems : 
1. loc, curr_loc etc should be in x,y format
2. Displaced by one row downwords not implemented yet. 
Pretty much everything after that is remaining.
"""


import random
import copy 


pieces = {
            # As all the other pieces are 2d arrays, one is also taken as a 2d array
            "one": [ ["*", "*", "*", "*"] ],
            "two": [ ["*", " ", " "], ["*", "*", "*"] ],
            "three": [ ["*", "*", "*"], ["*", " ", " "] ],
            "four": [ ["*", "*", " "], [" ", "*", "*"] ],
            "five": [ ["*", "*"], [ "*", "*"] ]
        }

board_width = 23
board_height = 21
board = []
game_over = False
row_left_offset = 2 # first index is row no, second index is *
row_right_offset = 1 # last index of a row is *
input_commands = ["a", "d", "w", "s"]


def initialise_board():

    board = [] # local variable
    
    for x in range(1, board_height+1):
        row = []

        # for adding the row no
        if x < 10:
            row.append(str(0) + str(x))
        else:
            row.append(str(x))

        for y in range(1, board_width):
            if x == board_height:
                row.append("*")
            elif y == 1 or y == board_width-1:
                row.append("*")
            else:
                row.append(" ")
        
        # print(row)
        # print(len(row))
        # print()
        board.append(row)

    return board
 

def print_string_board(board):
    string_board = ""

    for row in board:
        for item in row:
           string_board += item
        string_board += "\n" 
    
    print(string_board)


def get_random_piece():
    """ Returns a random key i.e. piece from pieces dictionary """
    piece = random.choice(list(pieces.keys()))
    return piece 


def get_starting_location_for_piece():
    """ Returns a random starting point for the piece to appear """
    """ Starting position cant be first, second or last index of the row array """
    """ RE: board_width-5, as last index of row array is *, and max width of a piece is 4; 1+4 = 5"""
    """ Ideally there should be a function to calcuate max width of the piece to do this dynamically """

    return random.randint(2, board_width-5)
    

def print_pieces():
    for piece in pieces:
        print()
        print(pieces[piece])
        print()


def place_piece_on_board(piece, loc):
    row = 0
    col = loc

    for x in range(row, len(pieces[piece])):
        for y in range(0, len(pieces[piece][x])):
            board[x][y+col] = pieces[piece][x][y]
    

def check_left_bounds(curr_piece, curr_loc):
    """ Returns True if the current piece is out of bounds of the board or False """

    if (curr_loc - row_left_offset) > 0:
        return False
    else:
        print()
        print("Outside left bounds error is immenent.")
        print()
        return True


def check_right_bounds(curr_piece, curr_loc):
    """ Returns True if the current piece is out of bounds of the board or False """

    length = len(pieces[curr_piece][0])

    if (curr_loc + row_right_offset + length) < board_width:
        return False
    else:
        print()
        print("Outside right bounds error is immenent.")
        print()
        return True


def check_overlap():
    """ Returns True if the current piece overlaps with previous pieces placed on the board """ 
    """ Otherwise False """
    return False


def move_piece_left(curr_piece, curr_loc):

    # Making a copy of the board so that it is intact from any changes.
    # Also useful for backtracking and showing the results of the last move
    copy_board = copy.deepcopy(board)

    bounds = check_left_bounds(curr_piece, curr_loc)
    overlap = check_overlap()

    if not bounds and not overlap:
        row_x = 0
        col_x = curr_loc

        for row in range(row_x + len(pieces[curr_piece])):
            for col in range(board_width):
                if (col >= col_x) and (col < (col_x + len(pieces[curr_piece][row]))):
                    # print(col, copy_board[row][col])
                    copy_board[row][col-1], copy_board[row][col] = copy_board[row][col], copy_board[row][col-1]

        # print_string_board(board)
        # print_string_board(copy_board)

        return True, board, copy_board
    else:
        return False 


def move_piece_right(curr_piece, curr_loc):

    # Making a copy of the board so that it is intact from any changes.
    # Also useful for backtracking and showing the results of the last move
    copy_board = copy.deepcopy(board)

    bounds = check_right_bounds(curr_piece, curr_loc)
    overlap = check_overlap()

    if not bounds and not overlap:
        # do the actual moving
        print("Moved Right")
        print(curr_piece)
        print(curr_loc)

        return True
    else:
        return False


def perform_user_action(user_input, curr_piece, curr_loc):
    """ Factory Method to perform the action """

    performed = False
    board = None
    copy_board = None
    str_user_input = str(user_input).strip().lower()

    if str_user_input == "a":
        performed, board, copy_board = move_piece_left(curr_piece, curr_loc)

    if str_user_input == "d":
        performed = move_piece_right(curr_piece, curr_loc)

    return performed, board, copy_board


def check_move_is_valid(user_input, curr_piece, curr_loc):
    performed, board, copy_board = perform_user_action(user_input, curr_piece, curr_loc)
    
    return performed, board, copy_board


def get_user_input(curr_piece, curr_loc):
    valid_input = False

    while(not valid_input):
        user_input = input("Your choices : \n" +
                           "a = move piece left \n" +
                           "d = move piece right \n" +
                           "w = rotate piece counter clockwise \n" +
                           "s = rotate piece clockwise \n")

        str_user_input = str(user_input).strip().lower()

        if str_user_input not in input_commands:
                # valid_input = False
                print()
                if str_user_input == "":
                    print("Input can't be empty Nigga")
                else:
                    print("{} is a wrong choice, ".format(user_input))

                print("Please select the right value from the list below.")
                continue
        else: 
            valid_input = True
            return str_user_input


def check_input(user_input, curr_piece, curr_loc):

    move_is_valid, board, copy_board = check_move_is_valid(user_input, curr_piece, curr_loc)
    
    if not move_is_valid:
        # valid_input = False
        print()
        print("{} is a right choice, ".format(user_input) +
                "but the move is not possible hence invalid move.")
    else:
       return move_is_valid, board, copy_board


def start_the_game():
    piece = get_random_piece()
    loc = get_starting_location_for_piece()

    place_piece_on_board(piece, loc)
    # place_piece_on_board("three", 5) # for testing
    print_string_board(board)

    # return 'three', 5 # for testing
    return piece, loc


def execute_move(copy_board):
    global board
    board = copy_board
    

def main():
    global board
    board = initialise_board()
    curr_piece, curr_loc = start_the_game()
    # print_pieces()

    # loop till gameover i.e. multiple loops for diff pieces and 
    # multiple loops for one piece till it get placed on teh board
    user_input = get_user_input(curr_piece, curr_loc)
    # print(user_input)
    move_is_valid, board, copy_board = check_input(user_input, curr_piece, curr_loc)
    
    if move_is_valid:
        execute_move(copy_board)

    print_string_board(board)
    
    #update curr_piece and curr_loc


if __name__ == "__main__":
    main()

